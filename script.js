let gameLevel;
let gameCompleted = false;
const scoreTemplate = document.getElementsByClassName("high-score");

window.onload = () => {
  let url = document.location.href;
  let level = url.split("?")[1].split("&")[1].split("=")[1];

  //
  scoreTemplate[0].innerHTML = `<h3>High Score :- ${localStorage.getItem(
    `highScore${level}`
  )}</h3>`;

  gameLevel = level;

  const imageNames = [
    "love.png",
    "perfect.png",
    "nerd.png",
    "shy.png",
    "loudspeaker.png",
    "workout.png",
    "happy.png",
    "in-love.png",
    "investment.png",
    "french.png",
    "outer-space.png",
    "social-media.png",
    "cauldron.png",
    "music.png",
    "crying.png",
    "broken-heart.png",
    "teddy-bear.png",
    "sad.png",
    "coffee.png",
    "poop.png",
    "death.png",
    "thumbs-down.png",
    "work-from-home.png",
    "celebration.png",
  ];

  // console.log(level)
  switch (level) {
    case "easy":
      shuffle(imageNames, 4);
      // console.log('level :>> ',level);
      break;
    case "medium":
      shuffle(imageNames, 12);
      // console.log(level);
      break;
    case "hard":
      shuffle(imageNames, 24);
      break;
  }
};
// console.log(window.currentLevel);









// shuffle(fileNames);
let totalNumberOfCardInGame = 0;
function shuffle(fileNames, length) {
  totalNumberOfCardInGame = length * 2; //for it filpedcard length reches this it means game is over
  let counter = fileNames.length;

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = fileNames[counter];
    fileNames[counter] = fileNames[index];
    fileNames[index] = temp;
  }
  let arr = fileNames.slice(0, length);

  let imgArray = arr.concat(arr);
  let lengthOfImgArray = imgArray.length;
  //shuffling imgArray
  while (lengthOfImgArray > 0) {
    index = Math.floor(Math.random() * lengthOfImgArray);
    lengthOfImgArray--;
    temp = imgArray[lengthOfImgArray];
    imgArray[lengthOfImgArray] = imgArray[index];
    imgArray[index] = temp;
  }

  createDivsForImages(imgArray);
}













let gameContainer = document.getElementsByClassName("memory-game")[0];
let scoreBox = document.getElementsByClassName("score")[0];
// console.log(scoreBox);
let id = 1;
function createDivsForImages(imgArray) {
  // console.log(gameContainer)
  gameContainer.id = "game-" + gameLevel;
  scoreBox.id = gameLevel;
  // console.log(gameLevel)
  for (let image of imgArray) {
    // console.log("Dasd")
    let newDiv = document.createElement("div");
    newDiv.className = "memory-card";
    newDiv.setAttribute("data-image", image);
    newDiv.id = id;
    id++;

    let imgFaceUp = document.createElement("img");
    imgFaceUp.className = "face-up";
    imgFaceUp.src = "./emoji/" + image;

    let imgFaceDown = document.createElement("img");
    imgFaceDown.className = "face-down";
    imgFaceDown.src = "./emoji/faceDown.png";
    // give it a class attribute for the value we are looping over
    newDiv.append(imgFaceUp);

    newDiv.appendChild(imgFaceDown);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}













let first, second;
let isFirstTime = false;
let count = 0;
let containIdForFlipCard = [];
// TODO: Implement this function!
function handleCardClick(event) {
  // console.log("this :>> ", this);
  // console.log(event);

  this.classList.toggle("flip");

  if (!isFirstTime) {
    isFirstTime = true;
    scoreRecord();
    first = this;
  } else {
    isFirstTime = false;
    second = this;
    if (first.id == second.id) {
      return;
    }
    scoreRecord();
    stopEventListeners();
    if (first.dataset.image === second.dataset.image) {
      first.removeEventListener("click", handleCardClick);
      containIdForFlipCard.push(first.id);
      // first.classList.add("noListener");
      second.removeEventListener("click", handleCardClick);
      containIdForFlipCard.push(second.id);
      // second.classList.add("noListener");
      startEventListners();
    } else {
      // gameContainer.freez();
      setTimeout(() => {
        first.classList.remove("flip");
        second.classList.remove("flip");
        startEventListners();
      }, 1500);
    }
  }
}








// createDivsForImages(shuffledImages);

function stopEventListeners() {
  gameContainer.childNodes.forEach((element) => {
    element.removeEventListener("click", handleCardClick);
  });
}








//gameContainers First not was undefind
let fistNode = true; //

function startEventListners() {
  // console.log('containIdForFlipCard :>> ', containIdForFlipCard);
  gameContainer.childNodes.forEach((element) => {
    if (!fistNode) {
      let elementId = element.id;
      // console.log(classId);
      if (!containIdForFlipCard.includes(elementId)) {
        element.addEventListener("click", handleCardClick);
      }
    }
    fistNode = false;
  });
  //checking if all the cards are fliped or not
  isGameCompleted();
}









let currentScore = 0;
const score = document.querySelector("#currentScore");
function scoreRecord() {
  // scoreElement.firstChild.remove();
  // console.log(score)
  score.textContent = currentScore;
  currentScore++;
}








const reset = document.getElementById("button");
reset.addEventListener("click", resetGame);
function resetGame() {
  location.href="index.html";
}

function isGameCompleted() {
  if (containIdForFlipCard.length == totalNumberOfCardInGame) {
    gameCompleted = true;
  }
  if (gameCompleted) {
    recordScore();
  }
}

function recordScore() {
  // console.log("asdss");
  let previousHighScore = localStorage.getItem(`highScore${gameLevel}`);
  if (previousHighScore === null) {
    previousHighScore = 9999;
    scoreTemplate[0].innerHTML = "<h3>High Score :-00</h3>";
  }
  if (parseInt(previousHighScore) > currentScore - 1) {
    // console.log(highScorePopup)
    console.log("funciton" + gameLevel);

    // highScorePopup.style.display = "block"
    localStorage.setItem(`highScore${gameLevel}`, currentScore);
    scoreTemplate[0].innerHTML = `<h3>High Score :- ${currentScore}</h3>`;
  }
}
